function logToConsole(message, group, type) {
    let allGroupsEnabled, enabledGroups, allGroupsDisabled, disabledGroups
    if(process.env.NEXT_PUBLIC_ENABLED_LOG_GROUPS) {
        allGroupsEnabled = process.env.NEXT_PUBLIC_ENABLED_LOG_GROUPS.toLowerCase() === "all"
        enabledGroups = allGroupsEnabled || process.env.NEXT_PUBLIC_ENABLED_LOG_GROUPS.toLowerCase() === "none" ? [] : process.env.NEXT_PUBLIC_ENABLED_LOG_GROUPS.split(",")
    } else {
        allGroupsEnabled = false
        enabledGroups = []
    }
    if(process.env.NEXT_PUBLIC_DISABLED_LOG_GROUPS) {
        allGroupsDisabled = process.env.NEXT_PUBLIC_DISABLED_LOG_GROUPS.toLowerCase() === "all"
        disabledGroups = allGroupsDisabled || process.env.NEXT_PUBLIC_DISABLED_LOG_GROUPS.toLowerCase() === "none" ? [] : process.env.NEXT_PUBLIC_DISABLED_LOG_GROUPS.split(",")
    } else {
        allGroupsDisabled = false
        disabledGroups = []
    }

    if( !allGroupsDisabled && !disabledGroups.includes(group) && ( allGroupsEnabled || enabledGroups.includes(group) ) ) {
        switch (type) {
            case "log":
                console.log(message)
                break
            case "warn":
                console.warn(message)
                break
            case "error":
                console.error(message)
                break
            default:
                throw `ERROR: Type ${type} unknown.`
        }
    }
}

const consoleSteroids = {}

consoleSteroids.log = function(message, group="default") {
    logToConsole(message, group, "log")
}

consoleSteroids.warn = function(message, group="default") {
    logToConsole(message, group, "warn")
}

consoleSteroids.error = function(message, group="default") {
    logToConsole(message, group, "error")
}

consoleSteroids.clear = console.clear
consoleSteroids.time = console.time
consoleSteroids.timeEnd = console.timeEnd
consoleSteroids.table = console.table
consoleSteroids.count = console.count
consoleSteroids.group = console.group
consoleSteroids.groupEnd = console.groupEnd

module.exports = consoleSteroids
